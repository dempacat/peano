use std::cmp::*;
use std::ops::*;

#[derive(Debug, Clone, Copy)]
/// Zero.
pub struct O();

#[derive(Debug, Clone, Copy)]
/// Succ of N.
pub struct S<N>(N);

/// Trait for natural number.
pub trait Nat {
    /// Get succ of self.
    fn s(self) -> S<Self> where Self: Sized { S(self) }
    /// Convert to u32.
    fn to_u32(self) -> u32;
}

impl Nat for O {
    fn to_u32(self) -> u32 { 0 }
}

impl<T: Nat> Nat for S<T> {
    fn to_u32(self) -> u32 { 1 + self.0.to_u32() }
}

// Add
impl<Rhs: Nat> Add<Rhs> for O {
    type Output = Rhs;

    fn add(self, rhs: Rhs) -> Self::Output {
        rhs
    }
}

impl<N: Add<Rhs>, Rhs: Nat> Add<Rhs> for S<N> {
    type Output = S<<N as Add<Rhs>>::Output>;

    fn add(self, rhs: Rhs) -> Self::Output {
        S(self.0 + rhs)
    }
}

// Mul
impl<Rhs: Nat> Mul<Rhs> for O {
    type Output = O;

    fn mul(self, _: Rhs) -> Self::Output {
        O()
    }
}

impl<N, Rhs> Mul<Rhs> for S<N>
        where N: Mul<Rhs>,
              Rhs: Nat + Copy,
              <N as std::ops::Mul<Rhs>>::Output: std::ops::Add<Rhs>
{
    type Output = <<N as Mul<Rhs>>::Output as Add<Rhs>>::Output;
    fn mul(self, rhs: Rhs) -> Self::Output {
        self.0 * rhs + rhs
    }

}

// PartialEq
impl PartialEq for O {
    fn eq(&self, _other: &O) -> bool { true }
}

impl<N> PartialEq<S<N>> for O {
    fn eq(&self, _other: &S<N>) -> bool { false }
}

impl<N> PartialEq<O> for S<N> {
    fn eq(&self, _other: &O) -> bool { false }
}

impl<N, M> PartialEq<S<M>> for S<N>
    where N: PartialEq<M>
{
    fn eq(&self, other: &S<M>) -> bool { self.0 == other.0 }
}

// PartialOrd
impl PartialOrd for O {
    fn partial_cmp(&self, _other: &O) -> Option<Ordering> {
        Some(Ordering::Equal)
    }
}

impl<N> PartialOrd<S<N>> for O {
    fn partial_cmp(&self, _other: &S<N>) -> Option<Ordering> {
        Some(Ordering::Less)
    }
}

impl<N> PartialOrd<O> for S<N> {
    fn partial_cmp(&self, _other: &O) -> Option<Ordering> {
        Some(Ordering::Greater)
    }
}

impl<N, M> PartialOrd<S<M>> for S<N>
    where N: PartialOrd<M>
{
    fn partial_cmp(&self, other: &S<M>) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

#[cfg(test)]
mod tests {
    use crate::{O, S, Nat};

    #[test]
    fn two_plus_three_equals_five() {
        #[allow(dead_code)]
        fn proof(
            lhs: S<S<O>>,
            rhs: S<S<S<O>>>,
        ) -> S<S<S<S<S<O>>>>> {
            lhs + rhs
        }
    }

    #[test]
    fn two_times_three_equals_six() {
        #[allow(dead_code)]
        fn proof(
            lhs: S<S<O>>,
            rhs: S<S<S<O>>>,
        ) -> S<S<S<S<S<S<O>>>>>> {
            lhs * rhs
        }
    }

    #[test]
    fn two_equals_two() {
        fn cmp(
            lhs: S<S<O>>,
            rhs: S<S<O>>,
        ) -> bool {
            lhs == rhs
        }
        assert!(cmp(O().s().s(), O().s().s()));
    }

    #[test]
    fn two_not_equals_three() {
        fn cmp(
            lhs: S<S<O>>,
            rhs: S<S<S<O>>>,
        ) -> bool {
            lhs != rhs
        }
        assert!(cmp(O().s().s(), O().s().s().s()));
    }

    #[test]
    fn two_less_than_three() {
        use std::cmp::Ordering;
        assert_eq!(O().s().s().partial_cmp(&O().s().s().s()), Some(Ordering::Less));
    }

    #[test]
    fn three_greater_than_two() {
        use std::cmp::Ordering;
        assert_eq!(O().s().s().s().partial_cmp(&O().s().s()), Some(Ordering::Greater));
    }
}

use peano::{Nat, O};

fn main() {
    let three = O().s().s().s();
    println!("{:?} = {}", three, three.to_u32());
}

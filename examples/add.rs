use peano::{O, Nat};

fn main() {
    let three = O().s().s().s();
    let two = O().s().s();
    println!("{:?} + {:?} = {:?}", three, two, three + two);
}
